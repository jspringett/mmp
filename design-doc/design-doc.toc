\contentsline {section}{\numberline {1}Software Specification}{2}
\contentsline {section}{\numberline {2}Design Overview}{2}
\contentsline {subsection}{\numberline {2.1}Introduction}{2}
\contentsline {subsection}{\numberline {2.2}Voting System}{2}
\contentsline {subsection}{\numberline {2.3}Voters}{2}
\contentsline {subsubsection}{\numberline {2.3.1}Waypoint Voter}{2}
\contentsline {subsubsection}{\numberline {2.3.2}Wind Voter}{2}
\contentsline {subsubsection}{\numberline {2.3.3}Channel Voter}{2}
\contentsline {subsubsection}{\numberline {2.3.4}Proximity Voter}{3}
\contentsline {subsubsection}{\numberline {2.3.5}Avoidance Voter}{3}
\contentsline {subsection}{\numberline {2.4}System Architecture}{3}
\contentsline {section}{\numberline {3}Detailed Software Design}{3}
\contentsline {subsection}{\numberline {3.1}Local Navigation Module}{3}
\contentsline {subsubsection}{\numberline {3.1.1}Vessel State Message}{4}
\contentsline {subsubsection}{\numberline {3.1.2}New Waypoint Message}{4}
\contentsline {subsubsection}{\numberline {3.1.3}Course Request Message}{4}
\contentsline {subsubsection}{\numberline {3.1.4}Desired Course Message}{5}
\contentsline {subsection}{\numberline {3.2}Course Ballot}{5}
\contentsline {subsubsection}{\numberline {3.2.1}Course Resolution}{5}
\contentsline {subsection}{\numberline {3.3}Arbiter}{5}
\contentsline {subsection}{\numberline {3.4}Voter Algorithims}{5}
\contentsline {subsubsection}{\numberline {3.4.1}Waypoint Voter}{6}
\contentsline {subsubsection}{\numberline {3.4.2}Wind Voter}{6}
\contentsline {subsubsection}{\numberline {3.4.3}Channel Voter}{7}
\contentsline {subsubsection}{\numberline {3.4.4}Proximity Voter}{8}
\contentsline {subsubsection}{\numberline {3.4.5}Avoidance Voter}{8}
\contentsline {subsection}{\numberline {3.5}Overall Class Diagram}{8}
